# Prometheus-Grafana stack

Installing prometheus stack using helm

```bash
# create namespace
kubectl apply -f prometheus-namespace.yaml
# add and update helm repo
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
# install prometheus stack
helm install -n monitoring-prometheus prometheus-stack prometheus-community/kube-prometheus-stack -f values.yaml
```

Wait for all pods to finish creating

```bash
kubectl get all -n monitoring-prometheus
```

Create service monitor

```bash
kubectl apply -f webapp-service-mon.yaml -n monitoring-webapp
```

Get username and password to access grafana

```bash
# username
echo -n "username: "; kubectl get secret prometheus-stack-grafana  -n monitoring-prometheus -o jsonpath="{.data.admin-user}" | base64 -d; echo
# password
echo -n "password: "; kubectl get secret prometheus-stack-grafana  -n monitoring-prometheus -o jsonpath="{.data.admin-password}" | base64 -d; echo
```

Access Grafana UI on port 3000

```bash
kubectl port-forward -n monitoring-prometheus services/prometheus-stack-grafana 3000:80
```

Access Prometheus UI on port 9090

```bash
kubectl port-forward -n monitoring-prometheus services/prometheus-stack-kube-prom-prometheus 9090
```
