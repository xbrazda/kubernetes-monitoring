import { createApp } from 'vue'
import App from './App.vue'
import { createRouter, createWebHistory } from "vue-router"
import './index.css'
import HostInfo from './components/HostInfo.vue';
import Cpu from './components/Cpu.vue'
import Memory from "./components/Memory.vue"
import Healthy from "./components/Healthy.vue"

const routes = [
    {path: "/", component: HostInfo},
    {path: "/host", component: HostInfo},
    {path: "/cpu", component: Cpu},
    {path: "/mem", component: Memory},
    {path: "/healthy", component: Healthy}
]

const router = createRouter({
    history: createWebHistory(),
    routes: routes
})

const app = createApp(App)

app.use(router)

app.mount("#app")