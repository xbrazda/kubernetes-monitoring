from flask import Flask
from flask_cors import CORS
from prometheus_flask_exporter import PrometheusMetrics


def create_app():
    from . import api
    from . import host, memory, cpu

    app = Flask(__name__)
    # register api endpoints to api
    api.bp.register_blueprint(host.bp)
    api.bp.register_blueprint(memory.bp)
    api.bp.register_blueprint(cpu.bp)
    # register api to app
    app.register_blueprint(api.bp)

    # allow CORS for testing
    CORS(app)

    # prometheus metrics at /metrics
    PrometheusMetrics(app)

    return app