from flask import Blueprint, request
from subprocess import run, Popen
from decimal import Decimal
from .utils import get_running_stress_processes, kill_process, get_stress_ng_executable_path
import string


bp = Blueprint("cpu", __name__, url_prefix="/cpu")

@bp.route("", methods=["GET"])
def info():
    cmd = "mpstat 1 1"
    cpu_usage_raw = run(cmd.split(),check=True, capture_output=True).stdout.decode().strip()
    avg_usage = cpu_usage_raw.split("\n")[-1]
    idle = Decimal(avg_usage.split()[-1].replace(",","."))
    used = Decimal(100) - idle
    return {
        "cpu_usage": float(used)
    }

@bp.route("/create", methods=["POST"])
def create():
    load = request.form.get("load")
    if load is None:
        return {"msg": "load is required field"}, 400

    try:
        l = int(load)
        assert 0 <= l <= 100
    except:
        return {"msg": "load needs to be an int in range 0 .. 100"}, 400

    stress_ng = get_stress_ng_executable_path()
    
    cmd = f"{stress_ng} -c 0 -l {load}"
    out = Popen(cmd.split())
    return {
        "pid": out.pid
        }

@bp.route("/running", methods=["GET"])
def running():
    return get_running_stress_processes("cpu")


@bp.route("/kill", methods=["POST"])
def kill():
    pid = request.form.get("pid")
    kill_all = request.form.get("all")
    processes = get_running_stress_processes("cpu")
    if kill_all == "true":
        for pid in processes.keys():
            kill_process(pid)
        return {"msg": "Success"}

    elif pid is None:
        return {"msg": "pid is required field"}, 400
    
    elif any(c not in string.digits for c in pid):
        return {"msg": "pid can only contain digits"}, 400
    
    elif pid not in processes.keys():
        return {"msg": "Not allowed to kill a non stress-ng process"}, 400
    
    else:
        kill_process(pid)
        return {"msg": "Success"}
