from flask import Blueprint, request

bp = Blueprint("api", __name__, url_prefix="/api")

@bp.route("", methods=["GET"])
def show_help():
    help = """<span>Avaliable endpoints
        ***
       /host 
        ***
        /memory
        ***
        /cpu
        ****
        /healthy
        </span>"""
    help = help.replace("\n", "<br>")
    return help


fail = False
@bp.route("/healthy", methods=["GET","POST"])
def healthy():
    global fail
    if request.method == "GET":
        if fail:
            return {"msg": "unhealthy"}, 500 
        else:
            return {"msg": "healthy"}, 200
    
    if request.method == "POST":
        req_fail = request.form.get("fail")
        if req_fail is None:
            return {"msg": "fail is required field"}, 400
        if req_fail != "true" and req_fail != "false":
            return {"msg": "fail needs to be set to 'true' or 'false'", "fail":req_fail}, 400
        fail = True if req_fail == "true" else False
        return {"msg":"success"}, 200
