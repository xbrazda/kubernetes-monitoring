from flask import Blueprint
from subprocess import run
import re
from opentelemetry import trace
from opentelemetry.exporter.jaeger.thrift import JaegerExporter
from opentelemetry.sdk.resources import SERVICE_NAME, Resource
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import BatchSpanProcessor

resource = Resource(attributes={
    SERVICE_NAME: "backend"
})

jaeger_exporter = JaegerExporter(
    agent_host_name="simplest-agent.observability.svc.cluster.local",
    agent_port=6831,
)

provider = TracerProvider(resource=resource)
processor = BatchSpanProcessor(jaeger_exporter)
provider.add_span_processor(processor)
trace.set_tracer_provider(provider)
tracer = trace.get_tracer(__name__)


bp = Blueprint("host", __name__, url_prefix="/host")


@bp.route("", methods=["GET"])
@tracer.start_as_current_span("GET /api/host")
def info():
    with tracer.start_as_current_span("whoami"):
        whoami_output = run("whoami", check=True, capture_output=True).stdout.decode().strip()

    with tracer.start_as_current_span("hostname"):
        hostname_output = run("hostname", check=True, capture_output=True).stdout.decode().strip()

    with tracer.start_as_current_span("os-release"):
        os_release = run(f"cat /etc/os-release".split(), check=True, capture_output=True).stdout.decode().strip()
        pretty_name = re.findall(r'PRETTY_NAME=\"(.*)\"', os_release)[0]
    
    return {
        "username": whoami_output,
        "hostname": hostname_output,
        "os-version": pretty_name,
        }