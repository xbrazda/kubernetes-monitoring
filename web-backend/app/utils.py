from subprocess import run
import re

def get_running_stress_processes(kind):
    cmd = "ps -e -o pid,args"

    ps = run(cmd.split(),check=True, capture_output=True).stdout.decode().strip()
    processes = {}
    for process in ps.split("\n"):
        stress_ng = get_stress_ng_executable_path()
        if kind == "mem":
            regex = r"([0-9]*) {} -vm 1 --vm-bytes (.*) --vm-hang 0".format(stress_ng)
        elif kind == "cpu":
            regex = r"([0-9]*) {} -c 0 -l ([0-9]*)".format(stress_ng)
        else:
            regex = ""
        output = re.findall(regex, process.strip())

        if output:
            pid, p_cmd = output[0]
            processes[pid] = p_cmd
    return processes

def kill_process(pid):
    cmd = f"kill -9 {pid}"
    run(cmd.split(), check=True)
    
def get_stress_ng_executable_path():
    return run("which stress-ng".split(),check=True, capture_output=True).stdout.decode().strip()