from flask import Blueprint, request
from subprocess import  Popen, run
import string
from .utils import get_running_stress_processes, kill_process, get_stress_ng_executable_path

bp = Blueprint("memory", __name__, url_prefix="/memory")

@bp.route("", methods=["GET"])
def info():
    # format in which stats should be displayed
    # default is kilobytes
    fmt = request.args.get("format")
    cmd = "free"
    if fmt == "kb":
        cmd += " -k"
    elif fmt == "mb":
        cmd += " -m"

    memory_stats_raw = run(cmd.split(), check=True, capture_output=True).stdout.decode().strip()
    memory_stats = memory_stats_raw.split("\n")[1]
    total, used, free, _, _, avaliable = memory_stats.split()[1:]
    
    return {
        "total_mem": total,
        "used_mem": used,
        "free_mem": free,
        "avaliable_mem": avaliable
    }
        

@bp.route("/create", methods=["POST"])
def create():
    size = request.form.get("size")
    if size is None:
        return {"msg": "size is required field"}, 400

    try:
        int(size)
    except:
        return {"msg": "size can only contain digits",}, 400
    
    fmt = request.form.get("format")
    formats = {
        "kb": "K",
        "mb": "M"
    }
    if fmt not in formats.keys():
        return {"msg": f"Invalid format. Allowed formats: {', '.join(formats.keys())}"}, 400
    
    formated_size = size + formats[fmt]

    stress_ng = get_stress_ng_executable_path()
    cmd = f"{stress_ng} -vm 1 --vm-bytes {formated_size} --vm-hang 0 --oomable"
    out = Popen(cmd.split())
    return {
        "pid": out.pid,
        "size": formated_size}


@bp.route("/running", methods=["GET"])
def running():
    return get_running_stress_processes("mem")


@bp.route("/kill", methods=["POST"])
def kill():
    pid = request.form.get("pid")
    kill_all = request.form.get("all")
    processes = get_running_stress_processes("mem")
    if kill_all == "true":
        for pid in processes.keys():
            kill_process(pid)
        return {"msg": "Success"}

    elif pid is None:
        return {"msg": "pid is required field"}, 400
    
    elif any(c not in string.digits for c in pid):
        return {"msg": "pid can only contain digits"}, 400
    
    elif pid not in processes.keys():
        return {"msg": "Not allowed to kill a non stress-ng process"}, 400
    
    else:
        kill_process(pid)
        return {"msg": "Success"}
