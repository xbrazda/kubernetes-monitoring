# Elastic stack

Install ECK

```bash
kubectl create -f https://download.elastic.co/downloads/eck/2.5.0/crds.yaml
kubectl apply -f https://download.elastic.co/downloads/eck/2.5.0/operator.yaml
```

Install all components of Elastic stack

```bash
kubectl apply -f . -n elastic-system
```

Check if all pods are started

```bash
kubectl get all -n elastic-system
```

Get credentials to access Kibana UI

```bash
echo -n "elastic:"; kubectl get secrets -n elastic-system elasticsearch-es-elastic-user -o jsonpath="{.data.elastic}" | base64 -d; echo 
```

Access Kibana UI

```bash
kubectl port-forward -n elastic-system svc/kibana-kb-http 5601
```
