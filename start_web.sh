minikube status
if [ $? -ne 0 ]
then
    minikube start
fi
eval $(minikube docker-env)

docker build -t web-backend web-backend 
docker build -t web-frontend web-frontend

# need to create namespace before applying rest of specs
kubectl apply -f web-k8s/web-namespace.yaml

kubectl apply -f web-k8s -n monitoring-webapp

echo "Webapp has finished building"
echo "Access webapp with command kubectl port-forward service/frontend-service YOUR_PORT:80 -n monitoring-webapp"