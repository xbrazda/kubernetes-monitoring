# install cert manager
kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.6.3/cert-manager.yaml
sleep 5 # allow time for all pods to start
kubectl create namespace observability 

kubectl create -f https://github.com/jaegertracing/jaeger-operator/releases/download/v1.40.0/jaeger-operator.yaml -n observability 
sleep 20 # allow time for all pods to start
kubectl apply -f simplest.yaml -n observability