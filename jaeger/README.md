# Jager

Install cert-manager

```bash
kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.6.3/cert-manager.yaml
```

Wait for all pods to start

```bash
kubectl get all -n cert-manager
```

Install Jager

```bash
kubectl create namespace observability 
kubectl create -f https://github.com/jaegertracing/jaeger-operator/releases/download/v1.40.0/jaeger-operator.yaml -n observability 
```

Start instance of Jager

```bash
kubectl apply -f simplest.yaml -n observability
```

Access the Jaeger UI

```bash
kubectl port-forward -n observability services/simplest-query 16686
```
