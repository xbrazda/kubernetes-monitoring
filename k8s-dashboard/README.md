# Kubernetes dashboard

Deploy k8s dashboard using remote yaml

```bash
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.7.0/aio/deploy/recommended.yaml
```

Wait for all pods to finish creating

```bash
kubectl get all -n kubernetes-dashboard
```

Start k8s proxy to access the dashboard

```bash
kubectl proxy
```

Dashboard will be avaliable at link <http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/>

To access the dashboard we need to create a token

First create an admin Service account

```bash
kubectl apply -f admin-user.yaml
kubectl apply -f admin-bind.yaml
```

Generate the token

```bash
kubectl -n kubernetes-dashboard create token admin-user
```
