import requests
import random

# Script for creating random traffic for fruit shop in target-3 namespace

fruits = ["apple", "banana", "orange", "strawberry"]

for _ in range(10):
    data = {"fruit": random.choice(fruits), "order_amount": random.randint(0,50)}
    requests.post("http://localhost:32003/order", data=data)

for _ in range(5):
    data = {"fruit": random.choice(fruits), "order_amount": random.randint(50,100)}
    requests.post("http://localhost:32003/order", data=data)

for _ in range(15):
    data = {"fruit": random.choice(fruits)}
    requests.post("http://localhost:32003/restock",data=data)

for _ in range(5):
    data = {"fruit": random.choice(fruits), "order_amount": random.randint(0,50)}
    requests.post("http://localhost:32003/order",data=data)

print("Traffic for fruit shop generated, check the dashboard")