#!/bin/sh
# store password for elastic user in variable
ELASTIC_PWD=$(k3s kubectl get secrets -n elastic-stack elasticsearch-es-elastic-user -o jsonpath='{.data.elastic}'| tr -d '"' | base64 -d)
# fruit shop dashboard
curl -k -XPOST "https://elastic:$ELASTIC_PWD@localhost:32000/api/saved_objects/_import?createNewCopies=true" -F 'file=@dashboards/fruit-shop.ndjson' -H "kbn-xsrf: true"
# ip traffic dashboard
curl -k -XPOST "https://elastic:$ELASTIC_PWD@localhost:32000/api/saved_objects/_import?createNewCopies=true" -F 'file=@dashboards/ip-traffic.ndjson' -H "kbn-xsrf: true"