#!/bin/bash
# store password for elastic user in variable
ELASTIC_PWD=$(k3s kubectl get secrets -n elastic-stack elasticsearch-es-elastic-user -o jsonpath='{.data.elastic}'| tr -d '"' | base64 -d)

# import pipeline
k3s kubectl exec -it -n elastic-stack elasticsearch-es-default-0 -c elasticsearch  -- curl -k -XPUT https://elastic:$ELASTIC_PWD@localhost:9200/_ingest/pipeline/logs-fruit_shop-default -H "Content-type: application/json" -d '
{ 
    "description": "Pipeline for fruit shop",
    "processors": 
    [
        {
            "grok": 
            {
                "field": "message",
                "patterns": 
                    [
                        "%{IPORHOST:order.source_ip} %{WORD:order.status} Ordered %{INT:order.amount} of %{WORD:order.fruit}",
                        "%{IPORHOST:order.source_ip} %{WORD:order.status} Tried to order %{INT:order.amount} of %{WORD:order.fruit} %{GREEDYDATA}",
                        "%{IPORHOST:restock.source_ip} %{WORD:restock.status} Restocked %{WORD:restock.fruit}"
                    ]
            }
        }
    ]
}'
