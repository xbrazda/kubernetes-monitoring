# target 1
k3s kubectl create ns target-1
k3s kubectl apply -f targets/target_1/wordpress
k3s kubectl apply -f targets/target_1/mysql_db
# target 2
k3s kubectl create ns target-2
k3s kubectl apply -f targets/target_2/flask_app
k3s kubectl apply -f targets/target_2/nginx
# target 3
k3s kubectl create ns target-3
k3s kubectl apply -f targets/target_3/mongo_db
k3s kubectl apply -f targets/target_3/node_app
