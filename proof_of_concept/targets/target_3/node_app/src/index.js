// connect to APM server
const apm = require("elastic-apm-node").start({
    serviceName: "fruit-shop",
    serverUrl: process.env.APM_SERVER_URL,
    verifyServerCert: false
})


const express = require("express")
const mongoose = require("mongoose")
const bodyParser = require("body-parser")

// initialize log file
const fs = require("fs")
const log_file = fs.createWriteStream("/var/log/fruit-shop.log")

const app = express()
app.set("view engine", "ejs")
app.use(express.static(__dirname + "/public"))
app.use(bodyParser.urlencoded({extended: true}))

const port = parseInt(process.env.PORT)

// connect to MongoDB
mongoose.connect(`mongodb://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_URL}/fruits`)

const fruitSchema = new mongoose.Schema({
    fruit: String,
    photo: String,
    count: Number
})

const Fruits = mongoose.model("fruits", fruitSchema)


app.get("/", async (req, res) => {
    const fruits = await Fruits.find({})
    res.render("index", {fruits: fruits})
})



// initialize metrics
var order_total = 0
var order_success = 0
var order_fail = 0
var restock_total = 0

var fruit_counts = {
    "banana": 0,
    "apple": 0,
    "orange": 0,
    "strawberry":0 
}

// fetch fruit counts every 10 seconds
setInterval(async () =>{
    const fruits = await Fruits.find({})
    fruits.forEach((fruit) => {
        fruit_counts[fruit.fruit] = fruit.count
    })
}, 10000)


app.post("/order", async (req, res) => {
    order_total += 1
    const amount_raw = req.body.order_amount
    const fruit = req.body.fruit
    try {
        const amount = parseInt(amount_raw)
        const avaliable = await Fruits.findOne({fruit: fruit})
        // user tried to order more fruit than is in stock
        if (amount > avaliable.count) {
            log_file.write(`${req.socket.remoteAddress} FAILURE Tried to order ${amount} of ${fruit} when ${avaliable.count} are available\n`)
        }
        else{
            // update database
            await Fruits.updateOne({fruit: fruit},
                {"$inc": {"count": - amount}})
            log_file.write(`${req.socket.remoteAddress} SUCCESS Ordered ${amount} of ${fruit}\n`)
            order_success += 1
        }
        
        
    }
    catch (err) {
        order_fail += 1
        log_file.write(err)
    }
    finally {
        res.redirect("/")
    }
})

app.post("/restock", async (req, res) => {
    restock_total += 1
    const fruit = req.body.fruit
    try {
        // restock 10 of fruit
        await Fruits.updateOne({fruit: fruit},
             {"$inc": {"count": 10}})
        log_file.write(`${req.socket.remoteAddress} SUCCESS Restocked ${fruit}\n`)
    }
    catch (err){
        log_file.write(err)
    }
    finally {
        res.redirect("/")
    }
})


// register order and restock metrics
apm.registerMetric("order.total", () =>{
    return order_total;
})

apm.registerMetric("order.success", () => {
    return order_success;
})

apm.registerMetric("order.fail", () =>{
    return order_fail;
})

apm.registerMetric("restock.total", () => {
    return restock_total;
})

// register fruit count metrics
Object.keys(fruit_counts).forEach((f) => {
    apm.registerMetric(`fruit.${f}`, () => {
        return fruit_counts[f]
    })
})


app.listen(port, () => {
    log_file.write(`App listening on port ${port}\n`)
})