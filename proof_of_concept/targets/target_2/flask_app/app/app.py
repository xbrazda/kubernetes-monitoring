from flask import Flask, send_from_directory, request
import random
import time
from elasticapm.contrib.flask import ElasticAPM
import requests
import os

# apm config
app = Flask(__name__)
app.config["ELASTIC_APM"] = {
    "SERVICE_NAME": "flask-app",
    # url of apm server service
    "SERVER_URL": os.environ["APM_SERVER"],
    # self signed cert
    "VERIFY_SERVER_CERT": "False"
}

apm = ElasticAPM(app)

@app.route("/")
def index():
    return send_from_directory("views", "index.html")

# global variable indicating current status
fail = True
@app.route("/status", methods= ["GET", "POST"])
def status():
    global fail
    # change status
    if request.method == "POST":
        status_arg = request.form["status"]
        if status_arg == "fail":
            fail = True
            return {"status": "fail"}
        if status_arg == "success":
            fail = False
            return {"status": "success"}
    # return status
    if request.method == "GET":
        if fail:
            return "Failure", 500
        else:
            return "Success", 200


@app.route("/transaction", methods = ["GET"])
# this endpoint simulates a slow/ not working service
def transaction():
    start = time.time()
    seconds = random.choice([0,1])
    miliseconds = random.uniform(0,1)
    # sleep for random amount of time
    time.sleep(seconds + miliseconds)
    # go to part-one endpoint
    requests.get(f"http://localhost:{os.environ['PORT']}/part-one")
    end = time.time()
    return f"Transaction started at {time.ctime(start)} and lasted {(end - start):.2f} seconds"


@app.route("/part-one", methods = ["GET"])
def part_one():
    cycles = random.randint(3,10)
    for i in range(cycles):
        # call part-two random number of times
        requests.get(f"http://localhost:{os.environ['PORT']}/part-two?i={i}")
    return {"part_one": "success"}


@app.route("/part-two", methods= ["GET"])
def part_two():

    i = int(request.args.get("i"))
    # fail every third request
    if i % 3 == 0:
        # sleep for short time
        time.sleep(random.uniform(0,0.3))
        return "fail", 500
    else:
        return "success", 200

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=int(os.environ['PORT']))     