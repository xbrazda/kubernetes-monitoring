### Structure
- *dashboards/* - contains custom dashboards
- *monitoring/* - configuration for Elastic stack and kube-state-metrics
- *scripts/* - scripts used to install components of the stack
- *targets/* - source code of sample applications

### Requirements
This proof of concept should be run on a Linux system with at least 10 GB of RAM and 20 GB of free space. The system should have `Docker` and `curl` installed, and the user should have root privileges.

### Deployment
To run this proof of concept, you need a Linux system with docker installed.

1. Install k3s and start the k3s service
```bash
curl -sfL https://get.k3s.io | K3S_KUBECONFIG_MODE="644"  sh -s - --docker
```
2. Install ECK
```bash
k3s kubectl apply -f monitoring/eck
```
3. Install kube-state-metrics
```bash
k3s kubectl apply -f monitoring/kube-state-metrics
```
4. Install targets by running the `install_targets.sh` script
```bash
scripts/install_targets.sh
```
5. Install Elastic stack by running the `install-elastic-stack.sh` script
```bash
scripts/install_elastic_stack.sh
```
6. Elastic stack will take a while to install. After a few minustes, check that all pods are ready. There should be 6 pods running (Elasticsearch, Kibana, APM server, Heartbeat, Elastic agent and Fleet server)
```bash
k3s kubectl get pod -n elastic-stack
```
7. Install custom ingest pipeline and dashboard by executing following scripts
```bash
scripts/fruit_shop_logs_pipeline.sh
scripts/dashboards.sh
```
8. Get password for the elastic user
```bash
k3s kubectl get secrets -n elastic-stack elasticsearch-es-elastic-user -o jsonpath='{.data.elastic}'| tr -d '"' | base64 -d
```
9. Access Kibana at https://localhost:32000, with username: elastic and password from last step
10. \(Optional\) Generate traffic to fruit shop
```bash
python3 scripts/traffic_fruit_shop.py
```

### Endpoints

| Name     | Address                |
|----------|------------------------|
| Kibana   | https://localhost:32000 | 
| Target-1 | http://localhost:32001 |
| Target-2 | http://localhost:32002 |
| Target-3 | http://localhost:32003 |

### Cleanup

Deploying this project is going to create a lot of containers, to delete the whole cluster, including the containers, run the script provided by k3s
```bash
k3s-uninstall.sh
```